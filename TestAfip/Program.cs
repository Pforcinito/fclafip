﻿using FCLAfip;
using System;
using System.Threading.Tasks;

namespace TestAfip
{
    class Program
    {
        static async Task Main(string[] args)
        {

            EngineInvoice engine = new EngineInvoice();
            var venta = new FCLAfip.Request.Venta()
            {
                QtyRegister = 1,
                TypeTicket = FCLAfip.Request.Venta.TypeTickets.B,
                TypeConcept = FCLAfip.Request.Venta.TypeConcepts.Product,
                TypeDocument = FCLAfip.Request.Venta.TypeDocuments.Nothing,
                Document = 0,
                ImporteNeto = 100,
                ImporteIva = 21,
                ImporteTotal = 121,
                PuntoVenta = 1,
                Fecha = DateTime.Now
            };
            var miFactura = await engine.SellInvoice(venta);
            venta.CAE = miFactura.Invoice.CAE;
            venta.NroComprobante = miFactura.Invoice.NumeroFactura;

            //QrSell qrSell = new QrSell();
            var qr = QrSell.GetQR(venta);


            Console.Write(qr);
            Console.Read();

        }
    }
}
