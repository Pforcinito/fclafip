﻿using FCLAData.Models;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace FCLAData
{
    public class DataEngine
    {
        private SanitariosContext _context;

        private readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public DataEngine(SanitariosContext context)
        {
            _context = context;
        }

        public LoginConfig GetConfigurationLogin()
        {
            log.Info("Get config by login in AFIP");
            try
            {
                var config = _context.LoginConfigs.FirstOrDefault();

                return config;
            }
            catch(Exception ex)
            {
                log.Error("Error in get config login: " , ex);
            }

            return null;
        }

        public LoginAfip GetLoginAFIP()
        {
            log.Info("Get Last login AFIP");
            try
            {
                var login = _context.LoginAfips.FirstOrDefault();

                return login;
            }
            catch (Exception ex)
            {
                log.Error("Error in get last login AFIP: ", ex);
            }

            return null;
        }

        public async Task SetLastLoginAFIP(LoginAfip login)
        {
            log.Info("Set last login AFIP");
            try
            {
                var lastLogin = _context.LoginAfips.FirstOrDefault();

                if(lastLogin != null)
                {
                    log.Info("add new credentials");

                    lastLogin.Sign = login.Sign;
                    lastLogin.Token = login.Token;
                    lastLogin.ExpirationDate = login.ExpirationDate;//soy un ganso

                    _context.Entry(lastLogin).State = Microsoft.EntityFrameworkCore.EntityState.Modified;                   
                }
                else
                {
                    log.Info("Don´t any data in database");

                    await _context.LoginAfips.AddAsync(login);

                    log.Info("add the first login");
                }

                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                log.Error("Error in set last login AFIP: ", ex);
            }
        }

    }
}
