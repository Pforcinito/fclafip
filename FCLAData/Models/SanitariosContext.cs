﻿using System;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace FCLAData.Models
{
    public partial class SanitariosContext : DbContext
    {
        public SanitariosContext()
        {
        }

        public SanitariosContext(DbContextOptions<SanitariosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<LoginAfip> LoginAfips { get; set; }
        public virtual DbSet<LoginConfig> LoginConfigs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#if DEBUG
                optionsBuilder.UseSqlServer("Server=DESKTOP-R3L94R9\\FORCIDB;Database=Sanitarios;Trusted_Connection=True;");
#else
                optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["CentralDB"].ConnectionString);
#endif

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<LoginAfip>(entity =>
            {
                entity.HasKey(e => e.Cuit);

                entity.ToTable("LoginAfip");

                entity.Property(e => e.Cuit)
                    .HasMaxLength(50)
                    .HasColumnName("CUIT");

                entity.Property(e => e.ExpirationDate).HasColumnType("date");
            });

            modelBuilder.Entity<LoginConfig>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("LoginConfig");

                entity.Property(e => e.Clave).HasMaxLength(50);

                entity.Property(e => e.Servicio).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
