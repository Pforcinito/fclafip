﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FCLAData.Models
{
    public partial class LoginConfig
    {
        public string Servicio { get; set; }
        public string CertPath { get; set; }
        public string Clave { get; set; }
        public string CUIT { get; set; } 
    }
}
