﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FCLAData.Models
{
    public partial class LoginAfip
    {
        public string Cuit { get; set; }
        public string Sign { get; set; }
        public string Token { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
