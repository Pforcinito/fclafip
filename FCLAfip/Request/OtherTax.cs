﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCLAfip.Request
{
    public class OtherTax
    {
        public short Id{ get; set; }
        public double BaseImp{ get; set; }
        public double Alic { get; set; }
        public string Desc{ get; set; }
        public double Importe { get; set; }

    }
}
