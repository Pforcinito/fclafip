﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCLAfip.Request
{
    public class Venta
    {
        public enum TypeTickets
        {
            A = 01,
            NCA = 03,
            B = 06,
            NCB = 08
        }

        public enum TypeConcepts
        {
            Product = 1,
            Service = 2,
            ProdAndService = 3
        }

        public enum TypeDocuments
        {
            CUIT = 80,
            DNI = 96,
            Nothing = 99 //para factura B
        }
        public int Id { get; set; }
        public long NroComprobante { get; set; }
        public int QtyRegister { get; set; } //cantidad de Facturas
        public TypeTickets TypeTicket { get; set; }
        public TypeConcepts TypeConcept { get; set; }
        public TypeDocuments TypeDocument { get; set; }
        public long Document { get; set; }
        public double ImporteNeto { get; set; }
        public double ImporteIva { get; set; }
        public double ImporteTotal { get; set; }        
        public List<OtherTax> OtherTaxes { get; set; }
        public int PuntoVenta { get; set; }
        public DateTime Fecha { get; set; }
        public string CAE { get;set; }


    }
}
