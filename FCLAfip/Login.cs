﻿using System;
using System.IO;
using System.Security;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using FCLAData;
using FCLAData.Models;

namespace FCLAfip
{
    public class Login
    {
        public string Sign { get; set; }
        public string Token { get; set; }
        public X509Certificate2 Certificate { get; set; }
        public string Serv { get; set; }
        public string Url { get; set; }
        public DateTime GenerationTime { get; set; }
        public DateTime ExpirationTime { get; set; }
        public XDocument XDocRequest { get; set; }
        public XDocument XDocResponse { get; set; }
        public string CertPath { get; set; }
        private SecureString Clave { get; set; }
        public string Cuit { get; set; }

        private int _globalId = 0;

        private FCLAData.DataEngine _dataEngine;

        public Login()
        {
            _dataEngine = new DataEngine(new FCLAData.Models.SanitariosContext());            
        }



        private async Task GetLoginAsync()
        {
            string cmsFirmadoBase64;
            //string loginTicketResponse;

            XmlNode uniqueIdNode;
            XmlNode generationTimeNode;
            XmlNode ExpirationTimeNode;
            XmlNode ServiceNode;

            _globalId += 1;

            GetConfigLogin();

            try
            {
                // Preparo el XML Request
                XmlDocument XmlLoginTicketRequest = new XmlDocument();
                XmlLoginTicketRequest.Load(/*Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"*/"LoginTemplate.xml");

                uniqueIdNode = XmlLoginTicketRequest.SelectSingleNode("//uniqueId");
                generationTimeNode = XmlLoginTicketRequest.SelectSingleNode("//generationTime");
                ExpirationTimeNode = XmlLoginTicketRequest.SelectSingleNode("//expirationTime");
                ServiceNode = XmlLoginTicketRequest.SelectSingleNode("//service");
                generationTimeNode.InnerText = DateTime.Now.AddMinutes(-10).ToString("s");
                ExpirationTimeNode.InnerText = DateTime.Now.AddMinutes(+10).ToString("s");
                uniqueIdNode.InnerText = Convert.ToString(_globalId);
                //serv= "wsfe" wsfev1
                ServiceNode.InnerText = Serv;

                //Obtenemos el Cert
                //path="C:\Users\ForciPC\source\repos\FCLAfip\CertificateTest.pfx"
                //Certificate = new X509Certificate2(File.ReadAllBytes(CertPath),Clave, X509KeyStorageFlags.PersistKeySet);
                if (Clave.IsReadOnly())
                    //Certificate.Import(File.ReadAllBytes(CertPath), Clave, X509KeyStorageFlags.PersistKeySet);
                    Certificate = new X509Certificate2(File.ReadAllBytes(CertPath), Clave, X509KeyStorageFlags.PersistKeySet);
                else
                    Certificate = new X509Certificate2(File.ReadAllBytes(CertPath));

                byte[] msgBytes = Encoding.UTF8.GetBytes(XmlLoginTicketRequest.OuterXml);

                //Firmamos
                ContentInfo infoContenido = new ContentInfo(msgBytes);
                SignedCms cmsFirmado = new SignedCms(infoContenido);

                CmsSigner cmsFirmante = new CmsSigner(Certificate);
                cmsFirmante.IncludeOption = X509IncludeOption.EndCertOnly;

                cmsFirmado.ComputeSignature(cmsFirmante);
                cmsFirmadoBase64 = Convert.ToBase64String(cmsFirmado.Encode());

                //Hago el login
                //url= "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL"
                //AFIP.TEST.LOGIN.LoginCMSService service = new AFIP.TEST.LOGIN.LoginCMSService();
                //service.Url = Url;

                LoginService.LoginCMSClient service = new LoginService.LoginCMSClient(LoginService.LoginCMSClient.EndpointConfiguration.LoginCms);
                
                
                var loginTicketResponse = await service.loginCmsAsync(cmsFirmadoBase64);

                // Analizamos la respuesta
                XmlDocument XmlLoginTicketResponse = new XmlDocument();
                XmlLoginTicketResponse.LoadXml(loginTicketResponse.loginCmsReturn);

                //Obtengo token y sign
                Token = XmlLoginTicketResponse.SelectSingleNode("//token").InnerText;
                Sign = XmlLoginTicketResponse.SelectSingleNode("//sign").InnerText;

                var exStr = XmlLoginTicketResponse.SelectSingleNode("//expirationTime").InnerText;
                var genStr = XmlLoginTicketResponse.SelectSingleNode("//generationTime").InnerText;
                ExpirationTime = DateTime.Parse(exStr).ToLocalTime();
                GenerationTime = DateTime.Parse(genStr).ToLocalTime();

                XDocRequest = XDocument.Parse(XmlLoginTicketRequest.OuterXml);
                XDocResponse = XDocument.Parse(XmlLoginTicketResponse.OuterXml);

                await _dataEngine.SetLastLoginAFIP(new LoginAfip { Cuit = Cuit, Sign = Sign, Token = Token, ExpirationDate = ExpirationTime });
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void MakeClave(string clave)
        {
            Clave = new SecureString();

            foreach (var charter in clave)
            {
                Clave.AppendChar(charter);
            }
            Clave.MakeReadOnly();
        }

        private void GetConfigLogin()
        {
            try
            {
                var config = _dataEngine.GetConfigurationLogin();
                Cuit = config.CUIT;
                Serv = config.Servicio;
                CertPath = config.CertPath;
                MakeClave(config.Clave);
            }
            catch(Exception ex)
            {
                throw (ex);
            }
                        
        }

        private LoginAfip GetLoginDB()
        {
            try
            {
                return _dataEngine.GetLoginAFIP();                
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public async Task GetLogin()
        {
            if (CheckLogin())
                return;

            try
            {
                var dbLogin = GetLoginDB();

                if(dbLogin == null || dbLogin.ExpirationDate < DateTime.Now.Date)
                {
                    await GetLoginAsync();
                }
                else
                {
                    Cuit = dbLogin.Cuit;
                    ExpirationTime = (DateTime)dbLogin.ExpirationDate;
                    Sign= dbLogin.Sign;
                    Token= dbLogin.Token;
                }

            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }

        public bool CheckLogin()
        {            
            if (Token == null || ExpirationTime < DateTime.Now)
                return false;

            return true; 
        }
    }
}
