﻿using AfipService;
using FCLAfip.Request;
using FCLAfip.Response;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;

namespace FCLAfip
{
    public class EngineInvoice
    {
        private Login _login;

        public EngineInvoice()
        {
            _login = new Login();
        }

        private FEAuthRequest  GetLogin()
        {
            var authRequest = new FEAuthRequest();
            //authRequest.Cuit = 20356454643;
            //authRequest.Sign = "GJb+cLXA1k/f42uqfp90k+olNC2LOary4MfM37jm9fepAswDpPayA5E2jiOkSc/25+lVij3Gfwar9SbI+v0SmPb6a1JZi6TDjBLxmvGFtfmFl/0Vj0XjALkZJjUNHW/OOrlJ854dE4iSE1pnxtwYRvrg6n12UJiN4KrPmOg8oNc=";
            //authRequest.Token = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iMjQ3ODE3NzkzMyIgZ2VuX3RpbWU9IjE2MjI0MTUxMjkiIGV4cF90aW1lPSIxNjIyNDU4Mzg5Ii8+CiAgICA8b3BlcmF0aW9uIHR5cGU9ImxvZ2luIiB2YWx1ZT0iZ3JhbnRlZCI+CiAgICAgICAgPGxvZ2luIGVudGl0eT0iMzM2OTM0NTAyMzkiIHNlcnZpY2U9IndzZmUiIHVpZD0iU0VSSUFMTlVNQkVSPUNVSVQgMjAzNTY0NTQ2NDMsIENOPTM1NjQ1NDY0IiBhdXRobWV0aG9kPSJjbXMiIHJlZ21ldGhvZD0iMjIiPgogICAgICAgICAgICA8cmVsYXRpb25zPgogICAgICAgICAgICAgICAgPHJlbGF0aW9uIGtleT0iMjAzNTY0NTQ2NDMiIHJlbHR5cGU9IjQiLz4KICAgICAgICAgICAgPC9yZWxhdGlvbnM+CiAgICAgICAgPC9sb2dpbj4KICAgIDwvb3BlcmF0aW9uPgo8L3Nzbz4K";

            authRequest.Cuit = Convert.ToInt64(_login.Cuit);
            authRequest.Sign = _login.Sign;
            authRequest.Token = _login.Token;

            return authRequest;
        }

        public async Task<ResponseInvoice> SellInvoice(Venta venta)
        {
            double importTributo = 0;

            await _login.GetLogin(); //siempre llamar a este metodo, ya que se encarga de realizar todos los controles al objeto login, asi solo le pega a la db y al servicio una unica vez.

            var authRequest = GetLogin();
            var service = new AfipService.ServiceSoapClient(ServiceSoapClient.EndpointConfiguration.ServiceSoap12);
            //service.Url = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL";
            //service.ClientCertificates.Add(_login.Certificate);

            FECAERequest req = new FECAERequest();
            FECAECabRequest cab = new FECAECabRequest();
            FECAEDetRequest det = new FECAEDetRequest();

#if(DEBUG)
            try
            {
                //1 = A, NCA = 3 - B =6 NCB = 8
                var tiposComprobantes = await service.FEParamGetTiposCbteAsync(authRequest);
                
                //0 = Productos
                var tiposConcepto = await service.FEParamGetTiposConceptoAsync(authRequest);
                //0 = CUIT - 9 = DNI
                var tipoDocumentos = await service.FEParamGetTiposDocAsync(authRequest);
                //Por el momento irrelevante
                var tiposOpcionales = await service.FEParamGetTiposOpcionalAsync(authRequest);
                //otros impuestos IIBB = 5, Percepcion IVA, etc
                var tiposTributos = await service.FEParamGetTiposTributosAsync(authRequest);
                //para este caso no es necesario (creo yo por el momento) 
                var tiposPostventa = await service.FEParamGetPtosVentaAsync(authRequest);
            }
            catch(Exception e)
            {
                throw (e);
            }
#endif
            cab.CantReg = 2;
            cab.PtoVta = venta.PuntoVenta;//1; //a chequear
            cab.CbteTipo = (int)venta.TypeTicket; //06;// Tipo comprobante
            req.FeCabReq = cab;

            FECAEDetRequest bar = det;

            bar.Concepto = (int)venta.TypeConcept; //1; Productos

            bar.DocTipo = (int)venta.TypeDocument; //99;Tipo CUIT
            bar.DocNro = venta.Document; //0;

            //Buscar el ultimo numero
            try
            {
                var lastRes = await service.FECompUltimoAutorizadoAsync(authRequest, venta.PuntoVenta, (int)venta.TypeTicket);                
                int last = lastRes.Body.FECompUltimoAutorizadoResult.CbteNro;

                bar.CbteDesde = last + 1; //para comprobantes A debe ser igual en ambos
                bar.CbteHasta = last + 1; //rango de facturas a pedir, en estae caso una sola
            }
            catch
            {
                bar.CbteDesde = 01;
                bar.CbteHasta = 01;
            }

            bar.CbteFch = venta.Fecha.ToString("yyyyMMdd");  //DateTime.Now.ToString("yyyyMMdd");//FechaDTP.Value.ToString("yyyyMMdd");

            bar.ImpNeto = venta.ImporteNeto; // Double.Parse(100.ToString("0.00"));
            bar.ImpIVA = venta.ImporteIva; //Double.Parse(21.ToString("0.00"));
            bar.ImpTotal = venta.ImporteTotal; //Double.Parse(121.ToString("0.00")); 

            if(venta.OtherTaxes != null)
            {
                bar.Tributos = new Tributo[venta.OtherTaxes.Count];
                int i = 0;
                venta.OtherTaxes.ForEach(t =>
                {
                    bar.Tributos[i] =  new Tributo { Alic = t.Alic, BaseImp = t.BaseImp, Desc = t.Desc, Id = t.Id, Importe = t.Importe} ; //para ingresos brutos IIBB
                    importTributo += t.Importe;
                    i++;                
                });
            }
           

            bar.ImpTotConc = 0;
            bar.ImpOpEx = 0;
            bar.ImpTrib = importTributo;

#if(DEBUG)

            var monedas = await service.FEParamGetTiposMonedasAsync(authRequest);
#endif
            bar.MonId = "PES";// mon.Id
            bar.MonCotiz = 1;


            AlicIva alicuota = new AlicIva();
            var iva = await service.FEParamGetTiposIvaAsync(authRequest);

            alicuota.Id = 5;// ivat.Id
            alicuota.BaseImp = venta.ImporteNeto; //100;
            alicuota.Importe = venta.ImporteIva;//Double.Parse(21.ToString("0.00"));

            bar.Iva = new AlicIva[] { alicuota };
            //bar.Iva.Append(alicuota);
            
            req.FeDetReq = new FECAEDetRequest[] { bar };
            


            try
            {
                var response = await service.FECAESolicitarAsync(authRequest, req);

                return ValidateResponse(response.Body.FECAESolicitarResult, venta.Id);                
            }
            catch(Exception  ex)
            {
                throw (ex);
            }           
        }

        public List<Task<ResponseInvoice>> SellInvoiceLote(List<Venta> ventas)
        {
            List<Task<ResponseInvoice>> listInvoice = new List<Task<ResponseInvoice>>();

            try
            {
                ventas.ForEach(v =>
                {
                    var res =  SellInvoice(v);
                    listInvoice.Add(res);
                });
            }
            catch(Exception ex)
            {
                throw (ex);
            }

            return listInvoice;
        }

        private ResponseInvoice ValidateResponse(FECAEResponse fECAE, int id)
        {
            ResponseInvoice response = new ResponseInvoice();
            response.Id = id;

            try
            {
                if (fECAE.FeCabResp.Resultado == "A")
                {
                    response.StatusInvoicem = ResponseInvoice.StatusInvoice.Aproved;
                }
                else
                {
                    response.StatusInvoicem = ResponseInvoice.StatusInvoice.Reproved;
                }

                if(fECAE.Errors != null)
                {
                    response.Error = fECAE.Errors[0].Msg;
                    return response;
                }

                if (fECAE.FeDetResp[0].Observaciones != null)
                {
                    response.Observation = fECAE.FeDetResp[0].Observaciones[0].Msg;
                }

                response.Invoice = new Invoice() { NumeroFactura = fECAE.FeDetResp[0].CbteHasta, CAE = fECAE.FeDetResp[0].CAE, Vto = DateTime.ParseExact(fECAE.FeDetResp[0].CAEFchVto.ToString(),"yyyyMMdd",CultureInfo.InvariantCulture) };

                return response;
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }
    }
}
