﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCLAfip.Qr.Model
{
    public class QRViewModel
    {
       public string  Ver {get;set;}
       public string Fecha { get; set; }
       public long  Cuit {get;set;}
       public int  PtoVta {get;set;}
       public int  TipoCmp {get;set;}
       public long  NroCmp {get;set;}
       public double  Importe{get;set;}
       public string  Moneda {get;set;}
       public int  Ctz {get;set;}
       public int  TipoDocRec{get;set;}
       public string  NroDocRec{get;set;}
       public int  TipoCodAut{get;set;}
       public string  CodAut{get;set;}
    }
}
