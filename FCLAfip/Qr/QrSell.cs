﻿using FCLAfip.Qr.Model;
using FCLAfip.Request;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace FCLAfip
{
    public static class QrSell
    {
        public static Bitmap GetQR(Venta venta)
        {
            Bitmap reto = null;
            string url = @"https://www.afip.gob.ar/fe/qr/?p=";
            string texto = CreateQRViewModel(venta);
            QRCoder.QRCodeGenerator generator = new QRCoder.QRCodeGenerator();
            QRCoder.QRCodeData codeData = generator.CreateQrCode(url + texto, QRCoder.QRCodeGenerator.ECCLevel.H);
            QRCoder.QRCode qrCodigo = new QRCoder.QRCode(codeData);

            reto = qrCodigo.GetGraphic(5, Color.Black, Color.White, true);
            using (var stream = new FileStream("qrcode.png", FileMode.Create))
                reto.Save(stream, ImageFormat.Png);


            return reto;
        }

        private static string CreateQRViewModel(Venta venta)
        {
            QRViewModel model = new QRViewModel();
            //var baseImp = Decimal.ToDouble(venta.Items.Sum(x => (x.Precio * x.Cantidad) - (x.Precio * x.Cantidad * (x.Bonificaciones / 100))));
            //var grupoIVA = venta.Items.GroupBy(d => d.Iva).Select(x => new { clave = x.Key, baseImp = Decimal.ToDouble((x.Sum(f => (f.Precio * f.Cantidad) - (f.Precio * f.Cantidad * (f.Bonificaciones / 100))))), Importe = Convert.ToDouble(x.Key - 1) * baseImp });
            //double importe = 0;
            //foreach (var grupoIvaItem in grupoIVA)
            //{
            //    importe += Convert.ToDouble(grupoIvaItem.clave - 1) * baseImp;
            //}

            model.Ver = "1";
            model.Fecha = venta.Fecha.ToString("yyyy-MM-dd");
            model.Cuit = venta.Document; /*"20171764905";*/
            model.PtoVta = venta.PuntoVenta;
            model.TipoCmp = (int)venta.TypeTicket;
            model.NroCmp = venta.NroComprobante;//venta.NumeroComprobateAFIP;
            model.Importe = (double)venta.ImporteTotal;// Math.Round(baseImp + importe, 2).ToString();
            model.Moneda = "PES";// venta.Moneda;
            model.Ctz = 1;//_FacturaElectronicaServices.GetCotizacion("1").ToString();
            model.TipoDocRec = (int)venta.TypeDocument;
            model.NroDocRec = venta.Document.ToString();
            model.TipoCodAut = (int)venta.TypeTicket;
            model.CodAut = venta.CAE;


            string modelConvertJason = JsonConvert.SerializeObject(model);

            var reto = System.Text.Encoding.UTF8.GetBytes(modelConvertJason);

            return System.Convert.ToBase64String(reto);

        }
    }
}
