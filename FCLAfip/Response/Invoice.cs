﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCLAfip.Response
{
    public class Invoice
    {
        public long NumeroFactura { get; set; }
        public string CAE { get; set; }
        public DateTime Vto { get; set; }

    }
}
