﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FCLAfip.Response
{
    public class ResponseInvoice
    {

        public enum StatusInvoice
        {
            Aproved,
            Reproved
        }
        public int Id { get; set; }
        public Invoice Invoice { get; set; }
        public StatusInvoice StatusInvoicem { get; set; }
        public string Observation { get; set; }
        public string Error { get; set; }
    }
}
